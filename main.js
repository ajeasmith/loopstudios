const openIcon = document.querySelector('#open');
const closeIcon = document.querySelector('#close');
const mobileMenu = document.querySelector('.mobile')
openIcon.addEventListener('click', openMenu)
closeIcon.addEventListener('click', openMenu)

function openMenu() {
    mobileMenu.classList.toggle('close');
}