# Frontend Mentor - Loopstudios landing page solution

This is a solution to the [Loopstudios landing page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/loopstudios-landing-page-N88J5Onjw). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents
- [The challenge](#the-challenge)
- [Screenshot](#screenshot)
- [Links](#links)
- [Built with](#built-with)
- [Author](#author)
### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page

### Screenshot

![Loopstudios landing page](https://i.ibb.co/GT1YWTz/Screen-Shot-2021-06-15-at-10-42-02-PM.png)
### Links

- Solution URL: [Add solution URL here](https://www.frontendmentor.io/solutions/loopstudios-landing-page-with-sass-and-javascript-EPE7z_oNf)
- Live Site URL: [Add live site URL here](https://loopstudios-seven-alpha.vercel.app/)
### Built with

- Semantic HTML5 markup
- Sass
- Flexbox
- Mobile-first workflow
- Javascript
## Author

- Website - [Ajea Smith](https://wdproject.vercel.app/)
- Frontend Mentor - [@ajeasmith](https://www.frontendmentor.io/profile/AjeaSmith)
- Twitter - [@ajeasmith](https://www.twitter.com/ajeasmith)
